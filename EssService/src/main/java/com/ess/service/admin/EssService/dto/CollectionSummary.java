package com.ess.service.admin.EssService.dto;

import java.util.List;

public class CollectionSummary {
    String name;
    long documentCount;
    String indexSize;
    List<String> shards;
    List<String> replicas;
    int segmentCount;
    String isOptimized;
    String lastModified;

    public CollectionSummary(String name, long documentCount, String indexSize, List<String> shards, List<String> replicas, int segmentCount, String isOptimized, String lastModified) {
        this.name = name;
        this.documentCount = documentCount;
        this.indexSize = indexSize;
        this.shards = shards;
        this.replicas = replicas;
        this.segmentCount = segmentCount;
        this.isOptimized = isOptimized;
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDocumentCount() {
        return documentCount;
    }

    public void setDocumentCount(long documentCount) {
        this.documentCount = documentCount;
    }

    public String getIndexSize() {
        return indexSize;
    }

    public void setIndexSize(String indexSize) {
        this.indexSize = indexSize;
    }

    public List<String> getShards() {
        return shards;
    }

    public void setShards(List<String> shards) {
        this.shards = shards;
    }

    public List<String> getReplicas() {
        return replicas;
    }

    public void setReplicas(List<String> replicas) {
        this.replicas = replicas;
    }

    public int getSegmentCount() {
        return segmentCount;
    }

    public void setSegmentCount(int segmentCount) {
        this.segmentCount = segmentCount;
    }

    public String getIsOptimized() {
        return isOptimized;
    }

    public void setIsOptimized(String isOptimized) {
        this.isOptimized = isOptimized;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
