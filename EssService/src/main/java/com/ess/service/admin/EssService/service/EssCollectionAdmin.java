package com.ess.service.admin.EssService.service;

import com.ess.service.admin.EssService.dto.CollectionSummary;
import com.ess.service.admin.EssService.exception.SolrCollectionException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import com.ess.service.admin.EssService.handler.EssResponseHandler;
import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.util.EssApplicationUtil;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.ClusterStateProvider;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.cloud.*;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.ReplicationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service(value="collectionAdmin")
public class EssCollectionAdmin implements EssAdminCollectionApi {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final JsonPointer INDEX_SIZE_POINTER = JsonPointer.compile("/details/indexSize");
    private static final String COLLECTIONS = "collections";
    private static final String LIVE_NODES = "live_nodes";
    private static final String ALIASES = "aliases";
    private static final Predicate<String> checkIsNullOrEmpty = s -> (s == null || s.isEmpty());
    private static final Predicate<String> checkNullAndEmpty = s -> ( s != null && !s.isEmpty());
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    CloudSolrClient cloudSolrClient;
    @Autowired
    SolrZkClient solrZkClient;
    @Autowired
    SolrClient solrClient;
    @Autowired
    EssResponseHandler essResponseHandler;
    @Autowired
    EssConfigAdmin essConfigAdmin;
    @Autowired
    EssCoreAdmin essCoreAdmin;

    @Value("${ess.solr.replication.command}")
    private String replicationCommand;

    @Value("${timestampFormat}")
    private String timestampFormat;

    /**
     * Fetches the list of collection summary details
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<CollectionSummary> getListOfCollections() throws IOException {
        try {
            final NamedList<Object> request = cloudSolrClient.request(new CollectionAdminRequest.List());
            List<Object> collectionList = (List<Object>) request.get(COLLECTIONS);
            return collectionList.stream().map(x -> x.toString()).filter(checkNullAndEmpty)
                    .map(this::getCollectionsSummary).filter(x -> x != null)
                    .collect(Collectors.toList());
        } catch (SolrServerException | IOException e) {
            log.error("{}",e);
            throw new IOException("Cannot get the list of collections");
        }
    }

    /**
     * Gets the collection summary from Solr.
     * @return CollectionSummary
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private CollectionSummary findCollectionSummary(String collectionName) throws IOException{
        CollectionSummary collectionSummary = null;
        try{
            if(!collectionExists(collectionName)) {
                String message = String.format("Collection %s does not exist",collectionName);
                throw new IOException(message);
            }else{
                long docCount = docCount(collectionName);
                String indexSize = getIndexSize(collectionName);
                List<String> replicas = new ArrayList<>();
                List<String> shards = new ArrayList<>();
                List<String> coreNames = new ArrayList<>();
                int segmentCount = 0;
                String isOptimized = "";
                String lastModified = "";
                Collection<Slice> slices = getSlices(collectionName);
                if(slices != null){
                    replicas = getReplicas(slices);
                    shards = getShards(slices);
                    coreNames = getCoresFromNode(slices);
                    if(coreNames != null) {
                        segmentCount = essCoreAdmin.getSegmentCountMultipleCores(coreNames);
                        isOptimized = essCoreAdmin.checkOptimized(coreNames.get(0));
                        lastModified = essCoreAdmin.getLastModified(coreNames.get(0));
                    }
                }
                collectionSummary = new CollectionSummary(collectionName, docCount, indexSize, shards, replicas, segmentCount, isOptimized, lastModified);
            }
        } catch (IOException e) {
            String message = String.format("Cannot fetch Collection Summary : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
        return collectionSummary;
    }

    /**
     * Fetches the list of collection summary details
     */
    @Override
    public ResponseEntity<?> listCollections() throws IOException {
        return essResponseHandler.sendSuccess(getListOfCollections());
    }

    /**
     * Fetches the summary details of a collection.
     */
    @Override
    public ResponseEntity<?> getCollectionSummary(String collectionName) throws IOException{
        return essResponseHandler.sendSuccess(findCollectionSummary(collectionName));
    }

    /**
     * Wrapper lambda function for getting list of collections
     */
    @SuppressWarnings({"unchecked","rawtypes"})

    private CollectionSummary getCollectionsSummary(String collectionName){
        try{
            return findCollectionSummary(collectionName);
        }catch (IOException e) {
            log.error("{}",e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Fetches the collection of Slice object
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private Collection<Slice> getSlices(String collectionName){
        return extractDocCollection(collectionName).getSlices();
    }

    /**
     * Fetches the list of replicas for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getReplicas(Collection<Slice> slices){
        return slices.stream().map(Slice::getReplicas).flatMap(x -> x.stream()).map(Replica::getName).collect(Collectors.toList());
    }

    /**
     * Fetches the list of replicas for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getCoresFromNode(Collection<Slice> slices){
        return slices.stream().map(Slice::getReplicas).flatMap(x -> x.stream()).filter(x -> getAliveOne().equalsIgnoreCase(x.getBaseUrl())).map(Replica::getCoreName).collect(Collectors.toList());
    }

    /**
     * Fetches the list of replicas for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private long getReplicaCount(Collection<Slice> slices){
        return slices.stream().map(Slice::getReplicas).flatMap(x -> x.stream()).map(Replica::getName).count();
    }

    /**
     * Fetches the list of replicas for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getReplicas(String collectionName) throws IOException{
        return getSlices(collectionName).stream().map(Slice::getReplicas).flatMap(x -> x.stream()).map(Replica::getName).collect(Collectors.toList());
    }

    /**
     * Fetches the leader replica of a shard in a collection.
     * @return Replica
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private Replica getLeaderReplica(String collectionName, String shardName) {
        return getSlices(collectionName).stream().filter(s -> shardName.equalsIgnoreCase(s.getName())).map(Slice::getLeader).findAny().get();
    }

    /**
     * Fetches the list of leader replicas of a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<Replica> getLeaderReplicas(String collectionName){
        return getSlices(collectionName).stream().map(Slice::getLeader).collect(Collectors.toList());
    }

    /**
     * Fetches the list of leader replica core names of a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getLeaderReplicaCoreNames(Collection<Slice> slices) {
        return slices.stream().map(Slice::getLeader).map(Replica::getCoreName).collect(Collectors.toList());
    }


    /**
     * Fetches the replicas for a given shard in a collection.
     * @return List<String> replica list from a shard
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getReplicasFromShard(String collectionName, String shardName){
        return getSlices(collectionName).stream().filter(s -> shardName.equalsIgnoreCase(s.getName()))
                .map(Slice::getReplicas).flatMap(x -> x.stream()).map(Replica::getName).collect(Collectors.toList());
    }

    /**
     * Fetches the list of shards for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getShards(Collection<Slice> slices){
        return slices.stream().map(Slice::getName).collect(Collectors.toList());
    }

    /**
     * Fetches the list of shards for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> getShards(String collectionName) throws IOException{
        return getSlices(collectionName).stream().map(Slice::getName).collect(Collectors.toList());
    }

    /**
     * Fetches the list of shards for a collection.
     * @return List<String>
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private long getShardCount(Collection<Slice> slices){
        return slices.stream().map(Slice::getName).count();
    }



    /**
     * Fetches the document count for a collection.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private long docCount(String collectionName) throws IOException{
        if(!collectionExists(collectionName)){
            String message = String.format("Collection %s does not exist",collectionName);
            log.error(message);
            throw new IOException(message);
        }
        cloudSolrClient.setDefaultCollection(collectionName);
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.set("q", "*:*");
        solrQuery.setRows(0);
        try{
            cloudSolrClient.commit();
            QueryResponse response = cloudSolrClient.query(solrQuery);
            return response.getResults().getNumFound();
        }catch (SolrServerException | IOException e) {
            String message = String.format("Cannot get document count of %s collection : %s",collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Fetches the index size of a collection.
     * Calls the replication handler of a collection.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private String getIndexSize(String collectionName) throws IOException{
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            HttpSolrClient client = (HttpSolrClient) solrClient;
            final String url = client.getBaseURL();
            final String formattedCollectionName = String.format("/%s", collectionName);
            final String formattedHandlerCommand = String.format("/%s", replicationCommand);
            final String replicationHandlerUrl = buildURIScheme(url, formattedCollectionName, formattedHandlerCommand, ReplicationHandler.CMD_DETAILS);
            String response = client.getHttpClient().execute(new HttpGet(replicationHandlerUrl), new BasicResponseHandler());
            JsonNode jsonResponse = mapper.readTree(response);
            return jsonResponse.at(INDEX_SIZE_POINTER).toString().replaceAll("\"","");
        }catch (IOException e){
            String message = String.format("Could not fetch index size of collection %s : %s",collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    private String buildURIScheme(String url, String collectionName, String handlerName, String handlerDetails){
        return new StringBuilder().append(url).append(collectionName).append(handlerName).append(handlerDetails).toString();
    }

    /**
     * Fetches the index size of a collection.
     * Calls the replication handler of a collection.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private DocCollection extractDocCollection(String collectionName){
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            ZkStateReader zkStateReader = new ZkStateReader(solrZkClient);
            zkStateReader.createClusterStateWatchersAndUpdate();                    // Ask -> Should I check for Alias?
            return zkStateReader.getClusterState().getCollection(collectionName);
        }catch (SolrException e) {
            log.error("{}",e);
            throw new IllegalArgumentException("Cannot find collection '" + collectionName + "' in ZooKeeper: " + e);
        }catch (Exception e) {
            throw new IllegalArgumentException("Cannot find expected information for SolrCloud in ZooKeeper: ", e);
        }
    }

    private String getAliveOne(){
        List<String> serverList;
        String baseUrl = null;
        HttpSolrClient client = (HttpSolrClient) solrClient;
        final String url = client.getBaseURL();
        ClusterStateProvider clusterStateProvider = cloudSolrClient.getClusterStateProvider();
        if(clusterStateProvider == null){
            throw new RuntimeException("The solr cloud client state provider is null");
        }
        clusterStateProvider.connect();
        //serverList = (List<String>) clusterStateProvider.getLiveNodes();
        for(String server : clusterStateProvider.getLiveNodes()){
            String port = server.split(":")[1].replace("_solr", "");
            if(url.contains(port)){
                baseUrl = liveNodeToUrl(server, clusterStateProvider);
                break;
            }
        }
        return baseUrl;
    }

    private String liveNodeToUrl(String server, ClusterStateProvider clusterStateProvider){
        final String url = server.replace("_", "/");
        String urlScheme = clusterStateProvider.getClusterProperty("urlScheme");
        log.info("urlScheme : "+ urlScheme);
        return (urlScheme != null)? String.format("%s://%s",urlScheme,url): String.format("http://%s",url);
    }

    /**
     * Checks if the collection exist in Solr.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private boolean collectionExists(String collectionName) throws IOException {
        try {
            final NamedList<Object> request = cloudSolrClient.request(new CollectionAdminRequest.List());
            return ((List) request.get(COLLECTIONS)).contains(collectionName);
        } catch (SolrServerException | IOException e) {
            throw new IOException("Cannot get the list of collections");
        }
    }

    /**
     * Create a new Alias for a collection
     * @param aliasName alias name is required for creation.
     * @param collectionName name of the collection.
     * @throws {@link IOException} is thrown when a problem while creating the alias.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> createAlias(String aliasName, String collectionName) throws IOException, SolrRequestException{
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(checkIsNullOrEmpty.test(aliasName)) {
                throw new SolrRequestException("Alias name cannot be null or empty");
            }
            CollectionAdminRequest.CreateAlias createAlias = CollectionAdminRequest.CreateAlias.createAlias(aliasName, collectionName);
            createAlias.process(cloudSolrClient);
            String response = String.format("Alias %s is created for collection : %s",aliasName,collectionName);
            return essResponseHandler.sendSuccess(response);
        } catch (IOException | SolrServerException e) {
            String message = String.format("Cannot create alias %s : %s",aliasName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Deletes the Alias for a collection
     * @param aliasName alias name is required for deletion.
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteAlias(String aliasName) throws IOException, SolrRequestException{
        try{
            if(checkIsNullOrEmpty.test(aliasName)) {
                throw new SolrRequestException("Alias name cannot be null or empty");
            }
            CollectionAdminRequest.DeleteAlias deleteAlias = CollectionAdminRequest.DeleteAlias.deleteAlias(aliasName);
            deleteAlias.process(cloudSolrClient);
            String response = String.format("Alias %s is deleted",aliasName);
            return essResponseHandler.sendSuccess(response);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot delete alias %s : %s",aliasName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Modifies the properties (metadata) on an alias. If a key is set with a value that is empty it will be removed.
     * @param aliasName alias name is required for modification.
     * @param propertyName name of the property to be modified
     * @param propertyValue value of the property.
     * @return CollectionAdminResponse
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> setAliasProperty(String aliasName, String propertyName, String propertyValue) throws IOException, SolrRequestException{
        boolean setProperty = true;
        try{
            if(checkIsNullOrEmpty.test(aliasName)) {
                throw new SolrRequestException("Alias name cannot be null or empty");
            }
            if(checkIsNullOrEmpty.test(propertyName)) {
                throw new SolrRequestException("Property key name cannot be null or empty");
            }
            if(propertyValue == null){
                propertyValue = "";
                setProperty = false;
            }
            CollectionAdminRequest.SetAliasProperty setAliasProperty = new CollectionAdminRequest.SetAliasProperty(aliasName);
            setAliasProperty.addProperty(propertyName, propertyValue).process(cloudSolrClient);
            String setPropResponse = String.format("Alias property %s is set to %s",propertyName,propertyValue);
            String deletePropResponse = String.format("Alias property %s is deleted",propertyName);
            return (setProperty) ? essResponseHandler.sendSuccess(setPropResponse):essResponseHandler.sendSuccess(deletePropResponse);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot set or delete alias property %s : %s",propertyName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Lists the Aliases present in SolrCloud
     * @return List of Aliases
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> listAlias() throws IOException{
        try{
            List<String> aliasList = new ArrayList();
            final NamedList<Object> response = cloudSolrClient.request(new CollectionAdminRequest.ListAliases());
            LinkedHashMap<String,String> aliasesMap =  (LinkedHashMap<String,String>) response.get(ALIASES);
            Set<Map.Entry<String,String>> aliasSet = aliasesMap.entrySet();
            for(Map.Entry<String, String> aliasIterator : aliasSet){
                StringBuilder builder = new StringBuilder();
                builder.append(aliasIterator.getKey());
                builder.append(" => ");
                builder.append(aliasIterator.getValue());
                aliasList.add(builder.toString());
            }
            return essResponseHandler.sendSuccess(aliasList);
        } catch (IOException | SolrServerException e) {
            throw new IOException("Cannot list Alias");
        }
    }

    /**
     * Get the clusterStatus of the SolrCloud instance
     * @return List of live nodes
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> getClusterStatus() throws IOException{
        return essResponseHandler.sendSuccess(getLiveNodes());
    }

    /**
     * Get the live nodes from the cluster object
     * @return List of live nodes
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<Object> getLiveNodes() throws IOException{
        try{
            final NamedList<Object> response = cloudSolrClient.request(new CollectionAdminRequest.ClusterStatus());
            final NamedList<Object> cluster = (NamedList<Object>) response.get("cluster");
            return (List<Object>) cluster.get(LIVE_NODES);
        } catch (IOException | SolrServerException e) {
            throw new IOException("Cannot fetch cluster status");
        }
    }

    /**
     * Check if collection exists in SolrCloud
     * Adds the replica to the shard
     * @param collectionName {@link String} name of the collection in which we are adding the replica.
     * @param shardName {@link String} name of the shard at which we are adding the replica
     * @throws {@link IOException} thrown if it is not possible to add the replica.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> addReplica(String collectionName, String shardName) throws IOException{
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist");
            }
            if(getReplicasFromShard(collectionName, shardName).size() < getLiveNodes().size()){
                CollectionAdminRequest.AddReplica addReplica = CollectionAdminRequest.AddReplica.addReplicaToShard(collectionName, shardName);
                addReplica.process(cloudSolrClient);
                String message = String.format("Added replica to %s of this collection %s ",shardName,collectionName);
                log.info(message);
                return essResponseHandler.sendSuccess(message);
            }else{
                String message = String.format("No available nodes to add in %s for collection %s",shardName,collectionName);
                log.info(message);
                return essResponseHandler.sendError(message);
            }
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot add replica to shard : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if collection exists in SolrCloud
     * Deletes the replica from the shard
     * @param collectionName {@link String} name of the collection from which we are deleting the replica.
     * @param shardName {@link String} name of the shard from which we are deleting the replica
     * @param replicaName {@link String} name of the replica to be deleted.
     * @throws {@link IOException} thrown if is not possible to delete the replica.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteReplica(String collectionName, String shardName, String replicaName) throws IOException{
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist");
            }
            if(!getReplicasFromShard(collectionName, shardName).contains(replicaName)){
                throw new IOException("Replica is not assigned to this shard");
            }else{
                CollectionAdminRequest.DeleteReplica deleteReplica = CollectionAdminRequest.DeleteReplica.deleteReplica(collectionName, shardName, replicaName);
                deleteReplica.process(cloudSolrClient);
                String message = String.format("Deleted replica %s from %s of this collection %s",replicaName,shardName,collectionName);
                log.info(message);
                return essResponseHandler.sendSuccess(message);
            }
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot delete replica to shard : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if collection exists in SolrCloud
     * Moves the replica to a target node
     * @param collectionName {@link String} name of the collection.
     * @param replicaName {@link String} name of the replica to be moved
     * @param targetNode {@link String} name of the target node eg 10.145.53.168:7574_solr
     * @throws {@link IOException} thrown if is not possible to move the replica.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> moveReplica(String collectionName, String replicaName, String targetNode) throws IOException{
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getReplicas(collectionName).contains(replicaName)){
                throw new IOException("Replica does not exist");
            }
            if(!getLiveNodes().contains(targetNode)){
                throw new IOException("Target Node is not alive");
            }
            CollectionAdminRequest.MoveReplica moveReplica = CollectionAdminRequest.MoveReplica.moveReplica(collectionName, replicaName, targetNode);
            moveReplica.process(cloudSolrClient);
            String message = String.format("Replica %s moved to target node ",replicaName,targetNode);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot move replica to target node : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if node is alive or not
     * Deletes all replicas from the node.
     * @param nodeName {@link String} name of the target node . eg 10.145.53.168:7574_solr
     * @throws {@link IOException} thrown if is not possible to delete all the replica.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteNode(String nodeName) throws IOException{
        try{
            if(!getLiveNodes().contains(nodeName)){
                throw new IOException("Node is not alive or does not exist");
            }
            CollectionAdminRequest.DeleteNode deleteNode = CollectionAdminRequest.DeleteNode.deleteNode(nodeName);
            deleteNode.process(cloudSolrClient);
            String message = String.format("All replicas from node %s is deleted",nodeName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot delete all replicas from the node target node %s : %s",nodeName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if nodes is alive and present
     * Move all replicas from the source node to target node.
     * @param sourceNode {@link String} name of the source node . eg 10.145.53.168:7574_solr
     * @param targetNode {@link String} name of the target node . eg 10.145.53.168:7574_solr
     * @throws {@link IOException} thrown if is not possible to replace all the replica.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> replaceNode(String sourceNode, String targetNode) throws IOException{
        try{
            if(!getLiveNodes().contains(sourceNode)){
                throw new IOException("Source Node is not alive or does not exist");
            }
            if(!getLiveNodes().contains(targetNode)){
                throw new IOException("Target Node is not alive or does not exist");
            }
            CollectionAdminRequest.ReplaceNode replaceNode = new CollectionAdminRequest.ReplaceNode(sourceNode, targetNode);
            replaceNode.process(cloudSolrClient);
            String message = String.format("All replicas from node %s is moved to node %s",sourceNode,targetNode);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot replace node %s to target node %s : %s",sourceNode,targetNode,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if nodes is alive and present
     * Move some replicas to the node as it is idle.
     * @param nodeName {@link String} name of the source node . eg 10.145.53.168:7574_solr
     * @throws {@link IOException} thrown if is not possible to utilize the node.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> utilizeNode(String nodeName) throws IOException{
        try{
            if(!getLiveNodes().contains(nodeName)){
                throw new IOException("Node is not alive or does not exist");
            }
            CollectionAdminRequest.UtilizeNode utilizeNode = new CollectionAdminRequest.UtilizeNode(nodeName);
            utilizeNode.process(cloudSolrClient);
            String message = String.format("Node %s is utilized", nodeName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot utilize the node %s : %s",nodeName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if collection is available in Zookeeper
     * Take a back up of the collection.
     * @param collectionName {@link String} name of the collection.
     * @param backupName {@link String} name of the backup.
     * @param path {@link String} name of the path.
     * @throws {@link IOException} thrown if is not possible to backup.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> backupCollection(String collectionName, String backupName, String path) throws IOException, SolrRequestException{
        try{
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(checkIsNullOrEmpty.test(backupName)) {
                throw new SolrRequestException("Back up name cannot be null or empty");
            }
            if(checkIsNullOrEmpty.test(path)) {
                throw new SolrRequestException("Back up path cannot be null or empty");
            }
            CollectionAdminRequest.Backup backup = new CollectionAdminRequest.Backup(collectionName, backupName);
            backup.setLocation(EssApplicationUtil.getPath(path));
            backup.process(cloudSolrClient);
            String message = String.format("Collection %s has been backup in this path %s",collectionName,path);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot backup the collection %s : %s",collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check if collection is available in Zookeeper
     * Restore the backup of the collection.
     * @param collectionName {@link String} name of the collection.
     * @param backupName {@link String} name of the backup.
     * @param path {@link String} name of the path.
     * @throws {@link IOException} thrown if is not possible to restore.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> restoreCollection(String collectionName, String backupName, String path) throws IOException, SolrRequestException{
        try{
            if(collectionExists(collectionName)){
                throw new IOException("Collection already exists in Solr");
            }
            if(checkIsNullOrEmpty.test(backupName)) {
                throw new SolrRequestException("Back up name cannot be null or empty");
            }
            if(checkIsNullOrEmpty.test(path)) {
                throw new SolrRequestException("Back up path cannot be null or empty");
            }
            CollectionAdminRequest.Restore restore = new CollectionAdminRequest.Restore(collectionName, backupName);
            restore.setLocation(EssApplicationUtil.getPath(path));
            restore.process(cloudSolrClient);
            String message = String.format("Collection %s has been restored back %s",collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        }catch (IOException | SolrServerException e) {
            String message = String.format("Cannot restore the collection %s from backup: %s",collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }


    /**
     * Reload collection configured in Solr instance
     * @return List<Object> collection meta info
     * @throws {@link IOException} is thrown when a problem with the solr request occurs.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> reloadCollection(String collectionName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            CollectionAdminRequest.Reload reload = CollectionAdminRequest.Reload.reloadCollection(collectionName);
            reload.process(cloudSolrClient);
            String message = String.format("Collection %s has been reloaded ",collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Cannot reload collection : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Force a leader in a particular shard in a collection.
     * @param collectionName {@link String} name of the collection.
     * @param shardName {@link String} name of the shard.
     * @throws {@link IOException} is thrown when a problem electing a leader.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> forceLeader(String collectionName, String shardName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist");
            }
            if(getLeaderReplica(collectionName, shardName) != null){
                String message = String.format("The shard already has an active leader. Force leader is not applicable");
                log.info(message);
                return essResponseHandler.sendError(message);
            }else{
                CollectionAdminRequest.ForceLeader reload = CollectionAdminRequest.ForceLeader.forceLeaderElection(collectionName, shardName);
                reload.process(cloudSolrClient);
                String message = String.format("Leader has been elected for shard %s in collection %s",shardName,collectionName);
                log.info(message);
                return essResponseHandler.sendSuccess(message);
            }
        } catch (SolrServerException | IOException e) {
            String message = String.format("Cannot elect leader for shard %s in collection %s : %s",shardName,collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Rebalance the leaders in a collection.
     * @param collectionName {@link String} name of the collection.
     * @throws {@link IOException} is thrown when a problem rebalancing a leader.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> rebalanceLearder(String collectionName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            CollectionAdminRequest.RebalanceLeaders rebalanceLeaders = new CollectionAdminRequest.RebalanceLeaders(collectionName);
            rebalanceLeaders.process(cloudSolrClient);
            String message = String.format("Leader replica has been rebalanced for collection %s",collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Leader replica cannot be rebalanced for collection  %s : %s",collectionName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Add a property to replica in a collection.
     * @param collectionName {@link String} name of the collection.
     * @param shardName {@link String} name of the shard.
     * @param replicaName {@link String} name of the replica.
     * @param propertyName {@link String} name of the property key.
     * @param propertyValue {@link String} name of the property value.
     * @throws {@link IOException} is thrown when a problem adding a replica property.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> addReplicaProperty(String collectionName, String shardName, String replicaName,
                                                String propertyName, String propertyValue) throws IOException, SolrRequestException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist");
            }
            if(!getReplicasFromShard(collectionName, shardName).contains(replicaName)){
                throw new IOException("Replica is not assigned to this shard");
            }
            if(checkIsNullOrEmpty.test(propertyName)) {
                throw new SolrRequestException("Property name cannot be null or empty");
            }
            CollectionAdminRequest.AddReplicaProp addReplicaProp = CollectionAdminRequest.AddReplicaProp.addReplicaProperty(collectionName, shardName, replicaName, propertyName, propertyValue);
            addReplicaProp.process(cloudSolrClient);
            String message = String.format("%s property is set", propertyName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Property %s cannot be set: %s",propertyName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Delete a property of replica in a collection.
     * @param collectionName {@link String} name of the collection.
     * @param shardName {@link String} name of the shard.
     * @param replicaName {@link String} name of the replica.
     * @param propertyName {@link String} name of the property key.
     * @throws {@link IOException} is thrown when a problem deleting a replica property.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteReplicaProperty(String collectionName, String shardName, String replicaName,
                                                   String propertyName) throws IOException, SolrRequestException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist");
            }
            if(!getReplicasFromShard(collectionName, shardName).contains(replicaName)){
                throw new IOException("Replica is not assigned to this shard");
            }
            if(checkIsNullOrEmpty.test(propertyName)) {
                throw new SolrRequestException("Property name cannot be null or empty");
            }
            CollectionAdminRequest.DeleteReplicaProp deleteReplicaProp = CollectionAdminRequest.DeleteReplicaProp.deleteReplicaProperty(collectionName, shardName, replicaName,propertyName);
            deleteReplicaProp.process(cloudSolrClient);
            String message = String.format("%s property is deleted", propertyName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Property %s cannot be deleted: %s",propertyName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Balance a property of replica in a collection.
     * @param collectionName {@link String} name of the collection.
     * @param propertyName {@link String} name of the property key.
     * @throws {@link IOException} is thrown when a problem balancing a replica property.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> balanceShardUnique(String collectionName, String propertyName) throws IOException, SolrRequestException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(checkIsNullOrEmpty.test(propertyName)) {
                throw new SolrRequestException("Property name cannot be null or empty");
            }
            CollectionAdminRequest.BalanceShardUnique balanceShardUnique = CollectionAdminRequest.BalanceShardUnique.balanceReplicaProperty(collectionName, propertyName);
            balanceShardUnique.process(cloudSolrClient);
            String message = String.format("%s property is balanced across all shards",propertyName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Property %s cannot be balanced: %s",propertyName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Migrate Cluster State
     * API is useful in migrating any collections created prior to Solr 5.0 to the more scalable cluster state format now used by default.
     * @param collectionName {@link String} name of the collection.
     * @throws {@link IOException} is thrown when a problem migrating the cluster state
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> migrateStateFormat(String collectionName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            CollectionAdminRequest.MigrateClusterState migrateClusterState = CollectionAdminRequest.MigrateClusterState.migrateCollectionFormat(collectionName);
            migrateClusterState.process(cloudSolrClient);
            String message = String.format("Cluster state of collection %s is migrated",collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Cluster state cannot be migrated: %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }


    /**
     * Deletes the stored response of an already failed or completed Asynchronous Collection API call.
     * @param requestId {@link String} name of the async request.
     * @throws {@link IOException} is thrown when a problem deleting the status.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteStatus(String requestId) throws IOException, SolrRequestException {
        try {
            if(checkIsNullOrEmpty.test(requestId)) {
                throw new SolrRequestException("Request id cannot be null or empty");
            }
            CollectionAdminRequest.DeleteStatus deleteStatus = CollectionAdminRequest.DeleteStatus.deleteAsyncId(requestId);
            deleteStatus.process(cloudSolrClient);
            String message = String.format("Status %s is deleted", requestId);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Status cannot be deleted: %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Assigns a role to a given node in the cluster. The only supported role is overseer
     * @param nodeName {@link String} name of the node name
     * @param roleName {@link String} name of the role to be added
     * @throws {@link IOException} is thrown when a problem while adding the role.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> addRole(String nodeName, String roleName) throws IOException, SolrRequestException {
        try {
            if(!getLiveNodes().contains(nodeName)){
                throw new IOException("Node is not alive or does not exist");
            }
            if(checkIsNullOrEmpty.test(roleName)) {
                throw new SolrRequestException("Role name cannot be null or empty");
            }
            CollectionAdminRequest.AddRole addRole = CollectionAdminRequest.AddRole.addRole(nodeName, roleName);
            addRole.process(cloudSolrClient);
            String message = String.format("Role %s is added to node %s",roleName,nodeName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Role cannot be added : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }


    /**
     * Remove an assigned role. This API is used to undo the roles assigned using ADDROLE operation
     * @param nodeName {@link String} name of the node name
     * @param roleName {@link String} name of the role to be added
     * @throws {@link IOException} is thrown when a problem while removing the role.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> removeRole(String nodeName, String roleName) throws IOException, SolrRequestException {
        try {
            if(!getLiveNodes().contains(nodeName)){
                throw new IOException("Node is not alive or does not exist");
            }
            if(checkIsNullOrEmpty.test(roleName)) {
                throw new SolrRequestException("Role name cannot be null or empty");
            }
            CollectionAdminRequest.RemoveRole removeRole = CollectionAdminRequest.RemoveRole.removeRole(nodeName, roleName);
            removeRole.process(cloudSolrClient);
            String message = String.format("Role %s is removed from node %s",roleName,nodeName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Role cannot be removed : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Add, edit or delete a collection property.
     * @param collectionName {@link String} name of the collection.
     * @param propertyName {@link String} name of the property.
     * @param propertyValue {@link String} value of the property.
     * @throws {@link IOException} is thrown when a problem with collection properties.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public CollectionAdminResponse collectionProperties(String collectionName, String propertyName, String propertyValue) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            CollectionAdminRequest.CollectionProp collectionProp = CollectionAdminRequest.CollectionProp.setCollectionProperty(collectionName, propertyName, propertyValue);
            return collectionProp.process(cloudSolrClient);
        } catch (SolrServerException | IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * Add, edit or delete a cluster property.
     * @param propertyName {@link String} name of the property.
     * @param propertyValue {@link String} value of the property.
     * @throws {@link IOException} is thrown when a problem with collection properties.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public CollectionAdminResponse clusterProperties(String propertyName, String propertyValue) throws IOException {
        try {
            CollectionAdminRequest.ClusterProp clusterProp = CollectionAdminRequest.ClusterProp.setClusterProperty(propertyName, propertyValue);
            return clusterProp.process(cloudSolrClient);
        } catch (SolrServerException | IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * The MIGRATE command is used to migrate all documents having a given routing key to another collection.
     * @param collectionName {@link String} name of the collection.
     * @param targetCollectionName {@link String} value of the target collection.
     * @param splitKey {@link String} The routing key prefix. For example, if the uniqueKey of a document is "a!123", then you would use split.key=a!. This parameter is required.
     * @throws {@link IOException} is thrown when a problem occur while migrating data.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> migrateData(String collectionName, String targetCollectionName, String splitKey) throws IOException, SolrRequestException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!collectionExists(targetCollectionName)){
                throw new IOException("Target Collection does not exist");
            }
            if(checkIsNullOrEmpty.test(splitKey)) {
                throw new SolrRequestException("Split key cannot be null or empty");
            }
            CollectionAdminRequest.Migrate migrate = CollectionAdminRequest.Migrate.migrateData(collectionName, targetCollectionName, splitKey);
            migrate.process(cloudSolrClient);
            String message = String.format("Documents has been migrated from %s to %s",collectionName,targetCollectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Documents cannot be migrated to target collection %s : %s",targetCollectionName, e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Delete the collection.
     * @param collectionName {@link String} The name of the collection to delete. This parameter is required.
     * @throws {@link IOException} is thrown when a problem while deleting the collection.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> deleteCollection(String collectionName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            CollectionAdminRequest.Delete delete = CollectionAdminRequest.Delete.deleteCollection(collectionName);
            delete.process(cloudSolrClient);
            String message = String.format("Collection %s has been deleted",collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Collection cannot be deleted : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Create a solr collection.
     * Check if collection name is already present in Zookeeper or not.
     * Upload Config set from Ess config repository.
     * If config set not uploaded, then download default config from Zookeeper and upload.
     * Check if numOfShards and numOfReplicas is less than 1.
     * Check is
     * @param collectionName {@link String} name of the collection to create.
     * @param numOfShards integer number of shards
     * @param replicationFactor integer number of replicas
     * @throws {@link IOException} thrown if is not possible to create the collection.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public ResponseEntity<?> createCollection(String collectionName, int numOfShards, int replicationFactor) throws IOException, SolrCollectionException, SolrRequestException {
        try {
            if(collectionName == null || collectionName.isEmpty()){
                throw new SolrRequestException("Collection name cannot be null or empty");
            }
            if(collectionExists(collectionName)){
                String message = String.format("Collection %s already exist in Solr",collectionName);
                throw new SolrCollectionException(message);
            }
            if(numOfShards < 1 ){
                throw new SolrRequestException("Shard count cannot be less than 1");
            }
            if(replicationFactor < 1 || replicationFactor > getLiveNodes().size()){
                throw new SolrRequestException("Replica count cannot be less than 1 and more than number of nodes in the cluster");
            }
            if(!essConfigAdmin.isConfigSetExist(collectionName)){
                essConfigAdmin.uploadConfigFromRepository(collectionName);
            }
            CollectionAdminRequest.Create create = CollectionAdminRequest.createCollection(collectionName, collectionName, numOfShards, replicationFactor);
            create.setReplicationFactor(replicationFactor);
            create.setMaxShardsPerNode(numOfShards);
            create.process(cloudSolrClient);
            if(collectionExists(collectionName)){
                String message = String.format("Collection %s has been created",collectionName);
                log.info(message);
                return essResponseHandler.sendSuccess(message);
            }else{
                String message = String.format("Collection %s cannot be created : Check Solr cluster",collectionName);
                log.info(message);
                return essResponseHandler.sendError(message);
            }
        } catch (IOException | SolrServerException e) {
            String message = String.format("Collection cannot be created : %s",e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Delete the shard from a collection.
     * @param collectionName {@link String} The name of the collection.
     * @param shardName {@link String} The name of the shard to be deleted.
     * @throws {@link IOException} is thrown when a problem while deleting the shard.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> deleteShard(String collectionName, String shardName) throws IOException {
        try {
            if(!collectionExists(collectionName)){
                String message = String.format("Collection %s does not exist",collectionName);
                log.error(message);
                throw new IOException(message);
            }
            if(!getShards(collectionName).contains(shardName)){
                throw new IOException("Shard does not exist in collection");
            }
            CollectionAdminRequest.DeleteShard deleteShard = CollectionAdminRequest.DeleteShard.deleteShard(collectionName, shardName);
            deleteShard.process(cloudSolrClient);
            String message = String.format("Shard %s has been deleted from collection %s",shardName,collectionName);
            log.info(message);
            return essResponseHandler.sendSuccess(message);
        } catch (SolrServerException | IOException e) {
            String message = String.format("Shard %s cannot be deleted : %s",shardName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

}
