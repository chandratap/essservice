package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

@RestController
@RequestMapping("/admin/replica")
public class EssAdminReplicaController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("collectionAdmin")
    EssAdminCollectionApi collectionApi;

    @DeleteMapping("/delete/deleteReplica")
    public ResponseEntity<?> deleteReplica(@RequestParam(required = true) String collectionName,
                                           @RequestParam(required = true) String shardName,
                                           @RequestParam(required = true) String replicaName) throws IOException {
        return collectionApi.deleteReplica(collectionName, shardName, replicaName);
    }

    @PostMapping("/create/addReplica")
    public ResponseEntity<?> addReplica(@RequestParam(required = true) String collectionName,
                                        @RequestParam(required = true) String shardName) throws IOException  {
        return collectionApi.addReplica(collectionName, shardName);
    }

    @GetMapping("/move/moveReplica")
    public ResponseEntity<?> moveReplica(@RequestParam(required = true) String collectionName,
                                         @RequestParam(required = true) String replicaName,
                                         @RequestParam(required = true) String targetNode) throws IOException  {
        return collectionApi.moveReplica(collectionName, replicaName, targetNode);
    }

    @PostMapping("/add/addReplicaProperty")
    public ResponseEntity<?> addReplicaProperty(@RequestParam(required = true) String collectionName,
                                                @RequestParam(required = true) String shardName,
                                                @RequestParam(required = true) String replicaName,
                                                @RequestParam(required = true) String propertyName,
                                                @RequestParam(required = true) String propertyValue) throws IOException, SolrRequestException {
        return collectionApi.addReplicaProperty(collectionName, shardName, replicaName, propertyName, propertyValue);
    }

    @DeleteMapping("/delete/deleteReplicaProperty")
    public ResponseEntity<?> deleteReplicaProperty(@RequestParam(required = true) String collectionName,
                                                   @RequestParam(required = true) String shardName,
                                                   @RequestParam(required = true) String replicaName,
                                                   @RequestParam(required = true) String propertyName) throws IOException, SolrRequestException  {
        return collectionApi.deleteReplicaProperty(collectionName, shardName, replicaName, propertyName);
    }
}
