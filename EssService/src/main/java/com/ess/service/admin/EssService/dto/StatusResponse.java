package com.ess.service.admin.EssService.dto;

import org.springframework.http.HttpStatus;

public class StatusResponse {
    private int statusCode;
    private HttpStatus statusMessage;

    public StatusResponse(int statusCode, HttpStatus statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(HttpStatus statusMessage) {
        this.statusMessage = statusMessage;
    }
}
