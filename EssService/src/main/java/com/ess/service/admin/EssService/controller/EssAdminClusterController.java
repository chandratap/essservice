package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

@RestController
@RequestMapping("/admin/cluster")
public class EssAdminClusterController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("collectionAdmin")
    EssAdminCollectionApi collectionApi;

    @GetMapping("/find/getClusterStatus")
    public ResponseEntity<?> getClusterStatus() throws IOException {
        return collectionApi.getClusterStatus();
    }

    @DeleteMapping("/delete/deleteNode")
    public ResponseEntity<?> deleteNode(@RequestParam(required = true) String nodeName) throws IOException  {
        return collectionApi.deleteNode(nodeName);
    }

    @GetMapping("/replace/replaceNode")
    public ResponseEntity<?> replaceNode(@RequestParam(required = true) String sourceNode,
                                         @RequestParam(required = true) String targetNode) throws IOException  {
        return collectionApi.replaceNode(sourceNode, targetNode);
    }

    @GetMapping("/utilizeNode")
    public ResponseEntity<?> utilizeNode(@RequestParam(required = true) String nodeName) throws IOException  {
        return collectionApi.utilizeNode(nodeName);
    }

    @DeleteMapping("/delete/deleteStatus")
    public ResponseEntity<?> deleteStatus(@RequestParam(required = true) String requestId) throws IOException, SolrRequestException {
        return collectionApi.deleteStatus(requestId);
    }

    @PostMapping("/add/addRole")
    public ResponseEntity<?> addRole(@RequestParam(required = true) String nodeName,
                                     @RequestParam(required = true) String roleName) throws IOException, SolrRequestException  {
        return collectionApi.addRole(nodeName, roleName);
    }

    @DeleteMapping("/remove/removeRole")
    public ResponseEntity<?> removeRole(@RequestParam(required = true) String nodeName,
                                        @RequestParam(required = true) String roleName) throws IOException, SolrRequestException  {
        return collectionApi.removeRole(nodeName, roleName);
    }

    @GetMapping("/set/forceLeader")
    public ResponseEntity<?> forceLeader(@RequestParam(required = true) String collectionName,
                                         @RequestParam(required = true) String shardName) throws IOException  {
        return collectionApi.forceLeader(collectionName, shardName);
    }

    @GetMapping("/set/rebalanceLearder")
    public ResponseEntity<?> rebalanceLearder(@RequestParam(required = true) String collectionName) throws IOException  {
        return collectionApi.rebalanceLearder(collectionName);
    }
}
