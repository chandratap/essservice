package com.ess.service.admin.EssService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EssServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EssServiceApplication.class, args);
	}

}
