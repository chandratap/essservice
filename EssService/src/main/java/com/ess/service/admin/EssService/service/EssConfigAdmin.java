package com.ess.service.admin.EssService.service;

import com.ess.service.admin.EssService.exception.SolrConfigException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import com.ess.service.admin.EssService.apis.EssAdminConfigApi;
import com.ess.service.admin.EssService.handler.EssResponseHandler;
import com.ess.service.admin.EssService.util.EssApplicationUtil;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.ZkClientClusterStateProvider;
import org.apache.solr.client.solrj.request.ConfigSetAdminRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service(value="configAdmin")
public class EssConfigAdmin implements EssAdminConfigApi {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String SOLR_CONFIG = "Solr-config";
    private static final String DEFAULT_SOLR_CONFIG = "_default";
    private static final String CONFIG_REPOSITORY = "./config/conf/";

    @Autowired
    CloudSolrClient cloudSolrClient;
    @Autowired
    ZkClientClusterStateProvider zkClientClusterStateProvider;
    @Autowired
    EssResponseHandler essResponseHandler;

    /**
     * Uploads a new config set to Zookeeper.
     * @throws {@link IOException} throws error if config set is already existing.
     */
    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> uploadConfig(String configSetName, String configSetPath) throws IOException, SolrConfigException, SolrRequestException {
        try {
            if(configSetName == null || "".equals(configSetName)) {
                throw new SolrRequestException("Config Set name cannot be null or empty");
            }
            if(configSetPath == null || "".equals(configSetPath)) {
                throw new SolrRequestException("Config Set path cannot be null or empty");
            }
            if(isConfigSetExist(configSetName)){
                String message = String.format("Config set %s already exists",configSetName);
                log.error(message);
                throw new SolrConfigException(message);
            }else{
                zkClientClusterStateProvider.uploadConfig(EssApplicationUtil.getAbsolutePath(configSetPath), configSetName);
                String response = String.format("Config set %s is uploaded to zookeeper",configSetName);
                log.info(response);
                return essResponseHandler.sendSuccess(response);
            }
        } catch (IOException e) {
            String message = String.format("Cannot upload config set %s to zookeeper : %s",configSetName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Uploads a new config set to Zookeeper.
     * @throws {@link IOException} throws error if config set is already existing.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    public void uploadConfigFromRepository(String configSetName) {
        try {
            if(!isConfigSetExist(configSetName)){
                zkClientClusterStateProvider.uploadConfig(EssApplicationUtil.getAbsolutePath(CONFIG_REPOSITORY), configSetName);
                if(!isConfigSetExist(configSetName)){
                    downloadAndUploadDefaultConfigSet(configSetName);
                }
            }
        } catch (IOException | SolrRequestException | SolrConfigException e) {
            log.error("{}",e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Updates a config set to Zookeeper.
     * @throws {@link IOException} throws error if config set is not Found.
     */
    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> updateConfig(String configSetName, String configSetPath) throws IOException, SolrRequestException, SolrConfigException{
        try {
            if(configSetName == null || "".equals(configSetName)) {
                throw new SolrRequestException("Config Set name cannot be null or empty");
            }
            if(configSetPath == null || "".equals(configSetPath)) {
                throw new SolrRequestException("Config Set path cannot be null or empty");
            }
            if(!isConfigSetExist(configSetName)){
                String message = String.format("Config set %s does not exist in Zookeeper",configSetName);
                log.error(message);
                throw new SolrConfigException(message);
            }else{
                zkClientClusterStateProvider.uploadConfig(EssApplicationUtil.getAbsolutePath(configSetPath), configSetName);
                String response = String.format("Config set %s is updated to zookeeper",configSetName);
                log.info(response);
                return essResponseHandler.sendSuccess(response);
            }
        } catch (IOException e) {
            String message = String.format("Cannot update config set %s to Zookeeper : %s",configSetName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Downloads the config set to Zookeeper.
     * @throws {@link IOException} throws error if config set is already existing.
     */
    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> downloadConfig(String configSetName, String configSetPath) throws IOException, SolrRequestException {
        try {
            if(configSetName == null || "".equals(configSetName)) {
                throw new SolrRequestException("Config Set name cannot be null or empty");
            }
            if(configSetPath == null || "".equals(configSetPath)) {
                throw new SolrRequestException("Config Set path cannot be null or empty");
            }
            if(isConfigSetExist(configSetName)){
                zkClientClusterStateProvider.downloadConfig(configSetName, EssApplicationUtil.getAbsolutePath(configSetPath));
                String response = String.format("Config set %s is downloaded to path %s", configSetName, configSetPath);
                log.info(response);
                return essResponseHandler.sendSuccess(response);
            }else{
                String message = String.format("Config set %s does not exist in Zookeeper",configSetName);
                log.error(message);
                throw new IOException(message);
            }
        } catch (IOException e) {
            String message = String.format("Cannot download config set %s from zookeeper : %s",configSetName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Checks a config set exist in Zookeeper.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    public boolean isConfigSetExist(String configSetName) throws IOException {
        return listConfigSets().contains(configSetName);
    }

    /**
     * Checks a config set exist in Zookeeper.
     */
    @Override
    public ResponseEntity<?> checkConfigSetExist(String configSetName) throws IOException, SolrRequestException{
        if(configSetName == null || "".equals(configSetName)) {
            throw new SolrRequestException("Config Set name cannot be null or empty");
        }
        String success = String.format("Config set %s exist",configSetName);
        String failure = String.format("Config set %s does not exist in cluster",configSetName);
        return (isConfigSetExist(configSetName)) ? essResponseHandler.sendSuccess(success):essResponseHandler.sendSuccess(failure);
    }

    /**
     * List the config set available in Zookeeper.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    private List<String> listConfigSets() throws IOException {
        ConfigSetAdminRequest.List list = new ConfigSetAdminRequest.List();
        try {
            return list.process(cloudSolrClient).getConfigSets();
        } catch (SolrServerException | IOException e) {
            throw new IOException("Cannot list config sets");
        }
    }

    /**
     * List the config set available in Zookeeper.
     */
    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> listConfigSet() throws IOException {
        return essResponseHandler.sendSuccess(listConfigSets());
    }

    /**
     * Check null value
     * @param param key
     * @param value value
     * @return value
     */
    private <T> T checkNull(String param, T value){
        if(value == null){
            throw new NullPointerException("Specify value for "+param);
        }
        return value;
    }


    /**
     * Delete a config set from Zookeeper cluster
     * @param configSetName {@link String} name of the configset to delete.
     * @throws {@link IOException} thrown if is not possible to delete the configset.
     */
    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ResponseEntity<?> deleteConfigSet(String configSetName) throws IOException, SolrRequestException {
        try {
            if(configSetName == null || "".equals(configSetName)) {
                throw new SolrRequestException("Config Set name cannot be null or empty");
            }
            if(!isConfigSetExist(configSetName)){
                String message = String.format("Config set %s does not exist in Zookeeper", configSetName);
                log.error(message);
                throw new IOException(message);
            }else{
                ConfigSetAdminRequest.Delete delete = new ConfigSetAdminRequest.Delete().setConfigSetName(configSetName);
                delete.process(cloudSolrClient);
                String response = String.format("Config set %s is deleted", configSetName);
                log.info(response);
                return essResponseHandler.sendSuccess(response);
            }
        } catch (SolrServerException | IOException e) {
            String message = String.format("Cannot delete configSet %s : %s", configSetName, e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    /**
     * Check and upload the configset to Zookeeper
     * @param configSetName {@link String} name of the configset to upload.
     * @throws {@link IOException} thrown if is not possible to delete the configset.
     */
    @SuppressWarnings({"unchecked","rawtypes"})
    public void downloadAndUploadDefaultConfigSet(String configSetName) throws IOException,SolrRequestException, SolrConfigException{
        Path temporaryPath = Files.createTempDirectory(SOLR_CONFIG);
        try {
            if(!isConfigSetExist(configSetName)){
                log.info("Temporary path {}",temporaryPath.toAbsolutePath().toString());
                downloadConfig(DEFAULT_SOLR_CONFIG, temporaryPath.toString());
                try{
                    Thread.sleep(10);
                }catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
                uploadConfig(configSetName, temporaryPath.toString());
             }
        }catch (IOException e) {
            String message = String.format("Cannot upload configSet %s : %s",configSetName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }finally {
            if(temporaryPath != null){
                EssApplicationUtil.deleteDirectory(temporaryPath.toString());
            }
        }
    }
}
