package com.ess.service.admin.EssService.dto;

public class EssAdminErrorResponse<T> {
    T status;

    public EssAdminErrorResponse(T status) {
        this.status = status;
    }

    public T getStatus() {
        return status;
    }

    public void setStatus(T status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EssAdminErrorResponse{" +
                "status=" + status +
                '}';
    }
}
