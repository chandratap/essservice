package com.ess.service.admin.EssService.handler;

import com.ess.service.admin.EssService.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.util.List;

@Service
public class EssResponseHandler {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public ResponseEntity<?> sendSuccess(HttpStatus status, String response) {
        return ResponseEntity.status(status).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body("{Response: \"" + response + "\"}");
    }

    public ResponseEntity<?> sendError(String response) {
        int statusCode = HttpStatus.BAD_REQUEST.value();
        HttpStatus statusMessage = HttpStatus.BAD_REQUEST;
        StatusResponse status = new StatusResponse(statusCode, statusMessage);
        EssAdminResponse essAdminResponse = new EssAdminResponse<String, StatusResponse>(response, status);
        return ResponseEntity.status(statusMessage).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body(essAdminResponse);
    }

    public ResponseEntity<?> sendError(HttpStatus status, String body) {
        return ResponseEntity.status(status).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body("{error: \"" + body + "\"}");
    }

    public ResponseEntity<?> sendError(ErrorDetails errorDetails, HttpStatus statusMessage) {
        EssAdminErrorResponse response = new EssAdminErrorResponse<ErrorDetails>(errorDetails);
        return ResponseEntity.status(statusMessage).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body(response);
    }

    public ResponseEntity<?> sendSuccess(List<?> list) {
        int statusCode = HttpStatus.OK.value();
        HttpStatus statusMessage = HttpStatus.OK;
        StatusResponse status = new StatusResponse(statusCode, statusMessage);
        EssAdminResponse response = new EssAdminResponse<List, StatusResponse>(list, status);
        return ResponseEntity.status(statusMessage).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body(response);
    }

    public ResponseEntity<?> sendSuccess(Object obj) {
        int statusCode = HttpStatus.OK.value();
        HttpStatus statusMessage = HttpStatus.OK;
        StatusResponse status = new StatusResponse(statusCode, statusMessage);
        EssAdminResponse response = new EssAdminResponse<Object, StatusResponse>(obj, status);
        return ResponseEntity.status(statusMessage).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body(response);
    }

    public ResponseEntity<?> sendSuccess(String message) {
        int statusCode = HttpStatus.OK.value();
        HttpStatus statusMessage = HttpStatus.OK;
        StatusResponse status = new StatusResponse(statusCode, statusMessage);
        EssAdminResponse response = new EssAdminResponse<String, StatusResponse>(message, status);
        return ResponseEntity.status(statusMessage).header("content-type", MediaType.APPLICATION_JSON_VALUE)
                .body(response);
    }
}
