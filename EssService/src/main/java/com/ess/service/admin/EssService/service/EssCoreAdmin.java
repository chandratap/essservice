package com.ess.service.admin.EssService.service;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

@Service
public class EssCoreAdmin {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String INDEX = "index";
    private static final String SEGMENT_COUNT = "segmentCount";
    private static final String CURRENT = "current";
    private static final String LAST_MODIFIED = "lastModified";
    private static final String NUM_DOCS = "numDocs";

    @Autowired
    SolrClient solrClient;
    @Autowired
    CloudSolrClient cloudSolrClient;

    public Integer getSegmentCount(String coreName) throws IOException {
        try{
            CoreAdminResponse coreAdminResponse = CoreAdminRequest.getStatus(coreName, solrClient);
            NamedList<Object> index = (NamedList<Object>)coreAdminResponse.getCoreStatus(coreName).get(INDEX);
            return (int) index.get(SEGMENT_COUNT);
        }catch (SolrServerException | IOException e) {
            String message = String.format("Cannot get Segment count of Core %s : %s",coreName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    public Integer getSegmentCountMultipleCores(List<String> coreNamesList) {
        return coreNamesList.stream().mapToInt(this::getSegmentsCount).sum();
    }

    private Integer getSegmentsCount(String corename){
        try{
            return getSegmentCount(corename);
        }catch (IOException e) {
            log.error("getSegmentsCount {}",e);
            throw new RuntimeException(e);
        }
    }

    public String checkOptimized(String coreName) throws IOException{
        String optimized = "";
        try{
            CoreAdminResponse coreAdminResponse = CoreAdminRequest.getStatus(coreName, solrClient);
            NamedList<Object> index = (NamedList<Object>) coreAdminResponse.getCoreStatus(coreName).get(INDEX);
            optimized = index.get(CURRENT).toString();
        }catch (NullPointerException e){
            log.error("{}",e);
            return optimized;
        }catch (SolrServerException | IOException e) {
            String message = String.format("Cannot get Optimized value of Core %s : %s",coreName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
        return optimized;
    }

    public String getLastModified(String coreName) throws IOException {
        try{
            String lastModified = "";
            CoreAdminResponse coreAdminResponse = CoreAdminRequest.getStatus(coreName, solrClient);
            NamedList<Object> index = (NamedList<Object>)coreAdminResponse.getCoreStatus(coreName).get(INDEX);
            if(getNumDocs(index) > 0){
                lastModified = index.get(LAST_MODIFIED).toString();
            }
            return lastModified;
        }catch (SolrServerException | IOException e) {
            String message = String.format("Cannot get last modified value of Core %s : %s",coreName,e.getMessage());
            log.error(message);
            throw new IOException(message);
        }
    }

    private int getNumDocs(NamedList<Object> index) {
        return (Integer)index.get(NUM_DOCS);
    }
}
