package com.ess.service.admin.EssService.apis;

import com.ess.service.admin.EssService.exception.SolrCollectionException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.*;

public interface EssAdminCollectionApi {

    public ResponseEntity<?> listCollections() throws IOException;

    public ResponseEntity<?> getCollectionSummary(String collectionName) throws IOException;

    public ResponseEntity<?> createAlias(String aliasName, String collectionName) throws IOException, SolrRequestException;

    public ResponseEntity<?> deleteAlias(String aliasName) throws IOException, SolrRequestException;

    public ResponseEntity<?> setAliasProperty(String aliasName, String propertyName, String propertyValue) throws IOException, SolrRequestException;

    public ResponseEntity<?> listAlias() throws IOException;

    public ResponseEntity<?> getClusterStatus() throws IOException;

    public ResponseEntity<?> addReplica(String collectionName, String shardName) throws IOException;

    public ResponseEntity<?> deleteReplica(String collectionName, String shardName, String replicaName) throws IOException;

    public ResponseEntity<?> moveReplica(String collectionName, String replicaName, String targetNode) throws IOException;

    public ResponseEntity<?> deleteNode(String nodeName) throws IOException;

    public ResponseEntity<?> replaceNode(String sourceNode, String targetNode) throws IOException;

    public ResponseEntity<?> utilizeNode(String nodeName) throws IOException;

    public ResponseEntity<?> backupCollection(String collectionName, String backupName, String path) throws IOException, SolrRequestException;

    public ResponseEntity<?> restoreCollection(String collectionName, String backupName, String path) throws IOException, SolrRequestException;

    public ResponseEntity<?> reloadCollection(String collectionName) throws IOException;

    public ResponseEntity<?> forceLeader(String collectionName, String shardName) throws IOException;

    public ResponseEntity<?> rebalanceLearder(String collectionName) throws IOException;

    public ResponseEntity<?> addReplicaProperty(String collectionName, String shardName, String replicaName,
                                                String propertyName, String propertyValue) throws IOException, SolrRequestException;

    public ResponseEntity<?> deleteReplicaProperty(String collectionName, String shardName, String replicaName,
                                                   String propertyName) throws IOException, SolrRequestException;

    public ResponseEntity<?> balanceShardUnique(String collectionName, String propertyName) throws IOException, SolrRequestException;

    public ResponseEntity<?> migrateStateFormat(String collectionName) throws IOException;

    public ResponseEntity<?> deleteStatus(String requestId) throws IOException, SolrRequestException;

    public ResponseEntity<?> addRole(String nodeName, String roleName) throws IOException, SolrRequestException;

    public ResponseEntity<?> removeRole(String nodeName, String roleName) throws IOException, SolrRequestException;

    public CollectionAdminResponse collectionProperties(String collectionName, String propertyName, String propertyValue) throws IOException;

    public CollectionAdminResponse clusterProperties(String propertyName, String propertyValue) throws IOException;

    public ResponseEntity<?> migrateData(String collectionName, String targetCollectionName, String splitKey) throws IOException, SolrRequestException;

    public ResponseEntity<?> deleteCollection(String collectionName) throws IOException;

    public ResponseEntity<?> createCollection(String collectionName, int numOfShards, int numOfReplicas) throws IOException, SolrCollectionException, SolrRequestException;

    public ResponseEntity<?> deleteShard(String collectionName, String shardName) throws IOException;
}
