package com.ess.service.admin.EssService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SolrRequestException extends Exception{
    public SolrRequestException(){
        super();
    }

    private static final long serialVersionUID = -8092173816915093856L;

    public SolrRequestException(String exceptionMessage) {
        super(exceptionMessage);
    }

    public SolrRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
