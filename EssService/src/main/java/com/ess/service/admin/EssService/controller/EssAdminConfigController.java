package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminConfigApi;
import com.ess.service.admin.EssService.exception.SolrConfigException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

@RestController
@RequestMapping("/admin/config")
public class EssAdminConfigController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("configAdmin")
    EssAdminConfigApi configApi;

    @PostMapping("/create/upload")
    public ResponseEntity<?> uploadConfig(@RequestParam(required = true) String configSetName,
                                          @RequestParam(required = true) String configSetPath) throws IOException, SolrConfigException, SolrRequestException {
        return configApi.uploadConfig(configSetName,configSetPath);
    }

    @PutMapping("/create/upload")
    public ResponseEntity<?> updateConfig(@RequestParam(required = true) String configSetName,
                                          @RequestParam(required = true) String configSetPath) throws IOException, SolrRequestException, SolrConfigException{
        return configApi.updateConfig(configSetName, configSetPath);
    }

    @GetMapping("/get/downloadConfigSet")
    public ResponseEntity<?> downloadConfig(@RequestParam(required = true) String configSetName,
                                            @RequestParam(required = true) String configSetPath) throws IOException, SolrRequestException{
        return configApi.downloadConfig(configSetName, configSetPath);
    }

    @GetMapping("/find/listConfigSets")
    public ResponseEntity<?> listConfigSets() throws IOException{
        return configApi.listConfigSet();
    }

    @GetMapping("/find/checkConfigSetExist")
    public ResponseEntity<?> checkConfigSetExist(@RequestParam(required = true) String configSetName) throws IOException, SolrRequestException{
        return configApi.checkConfigSetExist(configSetName);
    }

    @DeleteMapping("/delete/deleteConfigSet")
    public ResponseEntity<?> deleteConfigSet(@RequestParam(required = true) String configSetName) throws IOException, SolrRequestException{
        return configApi.deleteConfigSet(configSetName);
    }
}
