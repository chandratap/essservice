package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

@RestController
@RequestMapping("/admin/shard")
public class EssAdminShardController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("collectionAdmin")
    EssAdminCollectionApi collectionApi;

    @DeleteMapping("/delete/deleteShard")
    public ResponseEntity<?> deleteShard(@RequestParam(required = true) String collectionName,
                                         @RequestParam(required = true) String shardName) throws IOException {
        return collectionApi.deleteShard(collectionName, shardName);
    }

    @GetMapping("/reload/balanceShardUnique")
    public ResponseEntity<?> balanceShardUnique(@RequestParam(required = true) String collectionName,
                                                @RequestParam(required = true) String propertyName) throws IOException, SolrRequestException {
        return collectionApi.balanceShardUnique(collectionName, propertyName);
    }
}
