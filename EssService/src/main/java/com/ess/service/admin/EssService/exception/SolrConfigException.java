package com.ess.service.admin.EssService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SolrConfigException extends Exception{
    public SolrConfigException(){
        super();
    }

    private static final long serialVersionUID = -8092173816915093856L;

    public SolrConfigException(String exceptionMessage) {
        super(exceptionMessage);
    }

    public SolrConfigException(String message, Throwable cause) {
        super(message, cause);
    }
}
