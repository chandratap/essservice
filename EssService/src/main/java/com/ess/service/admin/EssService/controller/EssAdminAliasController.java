package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

@RestController
@RequestMapping("/admin/alias")
public class EssAdminAliasController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("collectionAdmin")
    EssAdminCollectionApi collectionApi;

    @PostMapping("/create/createAlias")
    public ResponseEntity<?> createAlias(@RequestParam(required = true) String aliasName,
                                         @RequestParam(required = true) String collectionName) throws IOException, SolrRequestException {
        return collectionApi.createAlias(aliasName, collectionName);
    }

    @DeleteMapping("/delete/deleteAlias")
    public ResponseEntity<?> deleteAlias(@RequestParam(required = true) String aliasName) throws IOException, SolrRequestException  {
        return collectionApi.deleteAlias(aliasName);
    }

    @PostMapping("/set/setAliasProperty")
    public ResponseEntity<?> setAliasProperty(@RequestParam(required = true) String aliasName,
                                                    @RequestParam(required = true) String propertyName,
                                                    String propertyValue) throws IOException, SolrRequestException  {
        return collectionApi.setAliasProperty(aliasName, propertyName, propertyValue);
    }

    @GetMapping("/find/listAlias")
    public ResponseEntity<?> listAlias() throws IOException  {
        return collectionApi.listAlias();
    }
}
