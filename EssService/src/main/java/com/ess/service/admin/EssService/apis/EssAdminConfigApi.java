package com.ess.service.admin.EssService.apis;

import org.springframework.http.ResponseEntity;
import com.ess.service.admin.EssService.exception.SolrConfigException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import java.io.IOException;
import java.util.List;


public interface EssAdminConfigApi {

    public ResponseEntity<?> uploadConfig(String configSetName, String configSetPath) throws IOException, SolrConfigException, SolrRequestException;

    public ResponseEntity<?> updateConfig(String configSetName, String configSetPath) throws IOException, SolrRequestException, SolrConfigException;

    public ResponseEntity<?> downloadConfig(String configSetName, String configSetPath) throws IOException, SolrRequestException;

    public ResponseEntity<?> checkConfigSetExist(String configSetName) throws IOException, SolrRequestException;

    public ResponseEntity<?> listConfigSet() throws IOException;

    public ResponseEntity<?> deleteConfigSet(String configSetName) throws IOException, SolrRequestException;

}
