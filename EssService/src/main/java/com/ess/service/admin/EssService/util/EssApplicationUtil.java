package com.ess.service.admin.EssService.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EssApplicationUtil {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static Path getAbsolutePath(String pathName){
        return Paths.get(pathName).toAbsolutePath();
    }

    public static String getPath(String pathName){
        return Paths.get(pathName).toAbsolutePath().toString();
    }

    public static String getCurrentTimeStamp(String timestampFormat){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(timestampFormat));
    }

    public static void deleteDirectory(String path){
        try{
            File directory = new File(path);
            if(directory.exists()) {
                delete(directory);
                log.info("Done deleted directory and folder {}",directory.getAbsolutePath());
            }
        }catch(Exception e){
            log.error("{}",e);
            throw new RuntimeException();
        }
    }

    public static void delete(File file){
        if(file.isDirectory()){
            if(file.list().length == 0){                      // for empty directory
                file.delete();
            }else{
                String files[] = file.list();
                for (String child : files) {
                    File deleteFile = new File(file, child);  // construct the file structure
                    delete(deleteFile);                       // recursive delete
                }
                if(file.list().length == 0){                  // check the directory again, if empty then delete it
                    file.delete();
                }
            }
        }else{
            file.delete();                                    // if file, then delete it
        }
    }
}
