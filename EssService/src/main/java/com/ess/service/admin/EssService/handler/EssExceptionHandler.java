package com.ess.service.admin.EssService.handler;

import com.ess.service.admin.EssService.dto.ErrorDetails;
import com.ess.service.admin.EssService.exception.SolrCollectionException;
import com.ess.service.admin.EssService.exception.SolrConfigException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import com.ess.service.admin.EssService.util.EssApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

@SuppressWarnings({"unchecked","rawtypes"})
@ControllerAdvice
public class EssExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${timestampFormat}")
    private String timestampFormat;
    @Autowired
    EssResponseHandler essResponseHandler;

    @ExceptionHandler(IOException.class)
    public final ResponseEntity<?> handleIOException(IOException ex, WebRequest request) {
        int statusCode = HttpStatus.NOT_FOUND.value();
        String timestamp = EssApplicationUtil.getCurrentTimeStamp(timestampFormat);
        String details = ex.getMessage();
        HttpStatus statusMessage = HttpStatus.NOT_FOUND;
        String localizedMessage = ex.getLocalizedMessage();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, details, statusMessage, localizedMessage);
        return essResponseHandler.sendError(errorDetails, statusMessage);
    }

    @ExceptionHandler(SolrConfigException.class)
    public final ResponseEntity<?> handleConfigUploadException(SolrConfigException ex, WebRequest request) {
        int statusCode = HttpStatus.CONFLICT.value();
        String timestamp = EssApplicationUtil.getCurrentTimeStamp(timestampFormat);
        String details = ex.getMessage();
        HttpStatus statusMessage = HttpStatus.CONFLICT;
        String localizedMessage = ex.getLocalizedMessage();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, details, statusMessage, localizedMessage);
        return essResponseHandler.sendError(errorDetails, statusMessage);
    }

    @ExceptionHandler(SolrCollectionException.class)
    public final ResponseEntity<?> handleSolrCollectionException(SolrCollectionException ex, WebRequest request) {
        int statusCode = HttpStatus.CONFLICT.value();
        String timestamp = EssApplicationUtil.getCurrentTimeStamp(timestampFormat);
        String details = ex.getMessage();
        HttpStatus statusMessage = HttpStatus.CONFLICT;
        String localizedMessage = ex.getLocalizedMessage();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, details, statusMessage, localizedMessage);
        return essResponseHandler.sendError(errorDetails, statusMessage);
    }

    @ExceptionHandler(SolrRequestException.class)
    public final ResponseEntity<?> handleSolrRequestException(SolrRequestException ex, WebRequest request) {
        int statusCode = HttpStatus.BAD_REQUEST.value();
        String timestamp = EssApplicationUtil.getCurrentTimeStamp(timestampFormat);
        String details = ex.getMessage();
        HttpStatus statusMessage = HttpStatus.BAD_REQUEST;
        String localizedMessage = ex.getLocalizedMessage();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, details, statusMessage, localizedMessage);
        return essResponseHandler.sendError(errorDetails, statusMessage);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
        int statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        String timestamp = EssApplicationUtil.getCurrentTimeStamp(timestampFormat);
        String details = ex.getMessage();
        HttpStatus statusMessage = HttpStatus.INTERNAL_SERVER_ERROR;
        String localizedMessage = ex.getLocalizedMessage();
        ErrorDetails errorDetails = new ErrorDetails(statusCode, timestamp, details, statusMessage, localizedMessage);
        return essResponseHandler.sendError(errorDetails, statusMessage);
    }

}
