package com.ess.service.admin.EssService.dto;

import org.springframework.http.HttpStatus;

public class ErrorDetails {
    private int statusCode;
    private String timestamp;
    private String details;
    private HttpStatus statusMessage;
    private String localizedMessage;


    public ErrorDetails(int statusCode, String timestamp, String details, HttpStatus statusMessage, String localizedMessage) {
        this.statusCode = statusCode;
        this.timestamp = timestamp;
        this.details = details;
        this.statusMessage = statusMessage;
        this.localizedMessage = localizedMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public HttpStatus getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(HttpStatus statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(String localizedMessage) {
        this.localizedMessage = localizedMessage;
    }
}
