package com.ess.service.admin.EssService.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.ZkClientClusterStateProvider;
import org.apache.solr.common.cloud.SolrZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Configuration
public class SolrConfig {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${spring.solr.host}")
    private String solrUrl;

    @Value("${solr.cloud.zkhosts}")
    private String zkHosts;

    @Value("${solr.cloud.zkChroot}")
    private String zkChroot;

    @Bean(name="solrClient")
    public SolrClient defaultSolrClient() {
        return solrClient();
    }

    @Bean(name="httpSolrClient")
    public SolrClient solrClient() {
        HttpSolrClient.Builder builder = new HttpSolrClient.Builder();
        builder.allowCompression(true);
        builder.withBaseSolrUrl(solrUrl);
        return builder.build();
    }

    @Bean(name="cloudSolrClient")
    public CloudSolrClient cloudSolrClient() {
        CloudSolrClient cloudSolrClient = null;
        if(zkHosts != null && !zkHosts.isEmpty()) {
            List<String> zkhosts = Arrays.asList(zkHosts.split(","));
            if(zkChroot != null && !zkChroot.isEmpty()) {
                cloudSolrClient = new CloudSolrClient.Builder(zkhosts, Optional.of(zkChroot)).build();
            }else {
                cloudSolrClient = new CloudSolrClient.Builder(zkhosts, Optional.<String>empty()).build();
            }
        }
        return cloudSolrClient;
    }

    @Bean(name="solrZkClient")
    public SolrZkClient getZkClient() {
        SolrZkClient zkClient = null;
        if(zkHosts != null && !zkHosts.isEmpty()) {
            zkClient = new SolrZkClient(zkHosts, 30000);
        }
        return zkClient;
    }

    @Bean
    public ZkClientClusterStateProvider getZookeeperClientProvider() {
        ZkClientClusterStateProvider zkClientClusterStateProvider = null;
        if (zkHosts != null && !zkHosts.isEmpty()) {
            zkClientClusterStateProvider = new ZkClientClusterStateProvider(zkHosts);
        }
        return zkClientClusterStateProvider;
    }

}
