package com.ess.service.admin.EssService.controller;

import com.ess.service.admin.EssService.apis.EssAdminCollectionApi;
import com.ess.service.admin.EssService.service.EssCollectionAdmin;
import com.ess.service.admin.EssService.exception.SolrCollectionException;
import com.ess.service.admin.EssService.exception.SolrRequestException;
import com.ess.service.admin.EssService.service.EssCoreAdmin;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

@RestController
@RequestMapping("/admin/collection")
public class EssAdminCollectionController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired(required=true)
    @Qualifier("collectionAdmin")
    EssAdminCollectionApi collectionApi;

    @GetMapping("/find/listCollections")
    public ResponseEntity<?> listCollections() throws IOException {
        return collectionApi.listCollections();
    }

    @GetMapping("/find/collectionSummary")
    public ResponseEntity<?> getCollectionSummary(@RequestParam(required = true) String collectionName) throws IOException{
        return collectionApi.getCollectionSummary(collectionName);
    }

    @PostMapping("/set/collectionProperties")
    public CollectionAdminResponse collectionProperties(@RequestParam(required = true) String collectionName,
                                                        @RequestParam(required = true) String propertyName,
                                                        String propertyValue) throws IOException  {
        return collectionApi.collectionProperties(collectionName, propertyName, propertyValue);
    }

    @GetMapping("/move/migrateData")
    public ResponseEntity<?> migrateData(@RequestParam(required = true) String collectionName,
                                         @RequestParam(required = true) String targetCollectionName,
                                         @RequestParam(required = true) String splitKey) throws IOException, SolrRequestException {
        return collectionApi.migrateData(collectionName, targetCollectionName, splitKey);
    }

    @DeleteMapping("/delete/deleteCollection")
    public ResponseEntity<?> deleteCollection(@RequestParam(required = true) String collectionName) throws IOException  {
        return collectionApi.deleteCollection(collectionName);
    }

    @PostMapping("/create/createCollection")
    public ResponseEntity createCollection(@RequestParam(required = true) String collectionName,
                                           @RequestParam(required = true) int numOfShards,
                                           @RequestParam(required = true) int numOfReplicas) throws IOException, SolrCollectionException, SolrRequestException {
        return collectionApi.createCollection(collectionName, numOfShards, numOfReplicas);
    }

    @GetMapping("/get/backupCollection")
    public ResponseEntity<?> backupCollection(@RequestParam(required = true) String collectionName,
                                              @RequestParam(required = true) String backupName,
                                              @RequestParam(required = true) String path) throws IOException, SolrRequestException  {
        return collectionApi.backupCollection(collectionName, backupName, path);
    }

    @PutMapping("/restore/restoreCollection")
    public ResponseEntity<?> restoreCollection(@RequestParam(required = true) String collectionName,
                                               @RequestParam(required = true) String backupName,
                                               @RequestParam(required = true) String path) throws IOException, SolrRequestException  {
        return collectionApi.restoreCollection(collectionName, backupName, path);
    }

    @GetMapping("/reload/reloadCollection")
    public ResponseEntity<?> reloadCollection(@RequestParam(required = true) String collectionName) throws IOException  {
        return collectionApi.reloadCollection(collectionName);
    }

    @GetMapping("/move/migrateStateFormat")
    public ResponseEntity<?> migrateStateFormat(@RequestParam(required = true) String collectionName) throws IOException  {
        return collectionApi.migrateStateFormat(collectionName);
    }

}
