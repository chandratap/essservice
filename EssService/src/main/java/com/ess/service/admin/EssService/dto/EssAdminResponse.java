package com.ess.service.admin.EssService.dto;

public class EssAdminResponse<T, U> {
    T response;
    U status;

    public EssAdminResponse(T response, U status) {
        this.response = response;
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public U getStatus() {
        return status;
    }

    public void setStatus(U status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EssResponse{" +
                "response=" + response +
                ", status=" + status +
                '}';
    }
}
